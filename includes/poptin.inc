<?php

function poptin_render_html(){
	poptin_redirect_link();
	$str = '';
	global $base_url;
	$deactivate_url = $base_url.'/poptindeactivate';
	$generateloginlink = $base_url.'/generateloginlink';
	$module_img_dir = $base_url.'/'.drupal_get_path('module', 'poptin') . '/images/';
	$semi_str = '';
	if(!is_poptin_user()){
		$regclass = '';
		$dashclass = 'hidediv';
	}else{
		$regclass = 'hidediv';
		$dashclass = '';
	}
		$semi_str = '<div class="poptin_forms poptin_registration '.$regclass.'">
		  <div class="pforms_wrap register_form">
			<div class="pforms_wrap_inner">
				<div class="pforms_head">
				   <ul>
					  <li>
						 <h3>Sign up for free</h3>
					  </li>
					  <li><a href="#" id="login_form">Already have an account?</a></li>
				   </ul>
				</div>
				<!--/pforms_head-->
				<div class="form_box">
				   <form method="post" id="popin_signup">
					  <input type="text" id="popin_email" class="input_box" name="email" placeholder="Enter your email">
					  <div class="bottom_form">
						 <input type="submit" value="Sign Up"  class="ppsubmit">
					  </div>
				   </form>
				</div>
			 </div>
			 <!--/pforms_wrap_inner-->        
		  </div>
			<!--/register_form-->
			<div class="pforms_wrap login_form" style="display:none;">
			 <div class="pforms_wrap_inner">
				<div class="pforms_head">
				   <ul>
					  <li>
						 <h3>Enter your user ID</h3>
					  </li>
					  <li><a href="#" id="register_form">Sign up for free</a></li>
				   </ul>
				</div>
				<!--/pforms_head-->
				<div class="form_box">
				   <form method="post" id="popin_id_register">
					  <input type="text" class="input_box" id="pop_up_id" placeholder="Enter your User ID" />
					  <div class="bottom_form">
						 <a class="wheremyid " href="javascript:void(0)" data-toggle="modal" >Where is my user ID? </a>
						 <input type="submit" value="Connect" class="pplogin" />                                        
					  </div>
				   </form>
				</div>
			 </div>			 
			 <!--/pforms_wrap_inner-->        
		  </div>
		  <!--/register_form-->
		</div>
		<div class="poptin_logged '.$dashclass.'">
		  <div class="poptin_logged_box">
			 <h2>You\'re all set!</h2>
			 <div class="tinyborder"></div>
			 <span class="everythinglooks">Click on the button below<br>to manage your poptins</span>
			 <img src="'.$module_img_dir.'vicon.png" alt="">
			 <a href="'.$generateloginlink.'" target="_blank"  class="ppcontrolpanel goto_dashboard_button_pp_updatable with_token">Go to Dashboard</a>
			 <a href="javascript:void(0)" class="pplogout">Deactivate Poptin</a>
		  </div>
		  <!--/poptin_logged_box-->
	   </div>';		
	$str = '<input id="baseurl" type="hidden" value="'.$base_url.'" /><div class="poptin-overlay" style="display: none;"></div><div id="poptin_main">
		<div class="poptinLogo"><img src="'.$module_img_dir.'poptinlogo.png" alt="" > </div>
		<!--/logo-->
		'.$semi_str.'	
		<!--/poptin_forms-->		
	   <div class="poptin_content <?php echo $sh_status; ?> poptin_registration">
		  <h2>Create your first poptin with ease</h2>
		  <div class="tinyborder"></div>
		  <div class="pvideo_box">
			 <div class="pvideo_box_in">                        <div style="width:100%;height:100%;width: 905px; height: 509px; float: none; clear: both; margin: 2px auto;">              <embed src="https://www.youtube.com/v/R_B8L3abt7Q?version=3&amp;hl=en_US&amp;rel=0&amp;showinfo=0" wmode="transparent" type="application/x-shockwave-flash" width="100%" height="100%" allowfullscreen="true" title="Adobe Flash Player">            </div>
			 </div>
			 <!--/pvideo_box_in--->
		  </div>
		  <!--/pvideo_box-->
	   </div>
	   <!--/poptin_content-->
	   <div class="poptin_content whychoose_sec <?php echo $sh_status; ?> poptin_registration">
		  <h2>Here\'s What Poptin Can Do For You</h2>
		  <div class="tinyborder"></div>
		  <div class="pcontent_in">
			 <div class="whychoose_list">
				<ul>
				   <li>
					  <div class="box boxEnv">
						 <div class="boxIcon"><img src="'.$module_img_dir.'envelope.png" alt="" /></div>
						 <h4>Get more email subscribers</h4>
					  </div>
				   </li>
				   <li>
					  <div class="box boxLeads">
						 <div class="boxIcon"><img src="'.$module_img_dir.'fanel.png" alt="" /></div>
						 <h4>Get more leads and sales</h4>
					  </div>
				   </li>
				   <li>
					  <div class="box boxCart">
						 <div class="boxIcon"><img src="'.$module_img_dir.'wheel.png" alt="" /></div>
						 <h4>Reduce shopping cart abandonment</h4>
					  </div>
				   </li>
				   <li>
					  <div class="box boxHeart">
						 <div class="boxIcon"><img src="'.$module_img_dir.'heart.png" alt="" /></div>
						 <h4>Increase visitors\' engagement</h4>
					  </div>
				   </li>
				</ul>
			 </div>
			 <!--/whychoose_list-->    
		  </div>
		  <!--/pcontent_in-->            	
	   </div>
	   <!--/poptin_content-->
	   <div class="poptin_content digital_marketers <?php echo $sh_status; ?> poptin_registration">
		  <h2>Digital Marketers ♥ Poptin</h2>
		  <div class="tinyborder"></div>
		  <div class="pcontent_in">
			 <div class="cfedbak_list">
				<ul>
				   <li>
					  <div class="box client1">
						 <div class="box_head">
							<img src="'.$module_img_dir.'profile1.png" alt=""  />
						 </div>
						 <!--head-->
						 <div class="clientboxtext">
							<h4>Michael Kamleitner</h4>
							<h6>CEO, Walls.io</h6>
							<p>Getting started with poptin was a breeze – we\'ve implemented the widget and connected it to our newsletter within minutes. Our conversion rate skyrocketed! </p>
						 </div>
					  </div>
				   </li>
				   <li>
					  <div class="box client2">
						 <div class="box_head">
							<img src="'.$module_img_dir.'profile2.png" alt=""  />
						 </div>
						 <!--head-->
						 <div class="clientboxtext">
							<h4>Deepak Shukla</h4>
							<h6>CEO, Purr Traffic</h6>
							<p>Been v.impressed with Poptin and the team behind it so far. Great responses times from support. The roadmap looks great. I highly recommend.  </p>
						 </div>
					  </div>
				   </li>
				   <li>
					  <div class="box client3">
						 <div class="box_head">
							<img src="'.$module_img_dir.'profile3.png" alt=""  />
						 </div>
						 <!--head-->
						 <div class="clientboxtext">
							<h4>Michael Voiskoun</h4>
							<h6>Marketing manager, Engie</h6>
							<p>It\'s super easy to use, doesn\'t require any prior coding skill. The team at Poptin is really helpful, providing great support, and adding always more features!  </p>
						 </div>
					  </div>
				   </li>
				</ul>
			 </div>
			 <!--/whychoose_list-->    
		  </div>
		  <!--/pcontent_in-->            	
	   </div>
	   <!--/poptin_content-->
	    <div class="poptin_content clients_feedback">
		  <h2>Let us know what you think <img class="emoji" src="'.$module_img_dir.'1f642.svg" alt=""  /></h2>
		  <div class="tinyborder"></div>
		  <div class="reviewbox">
			 <div class="reviewtitle">If Poptin already helped you to grow your business, please click on the button below and leave a positive review <img class="emoji" src="'.$module_img_dir.'1f642.svg" alt=""  /></div>
			 <div class="reviewstars"><img src="'.$module_img_dir.'stars.png" alt=""  /></div>
			 <div class="reviewlink"><a href="https://www.drupal.org/project/poptin" target="_blank">Write a Review</a></div>
		  </div>
		  <!--/reviewbox-->            	
	   </div>
	   <!--/poptin_content-->
	   <div class="poptin_footer">
		  <div class="poptin_footer_img">
			 <img class="normal_img parrot" src="'.$module_img_dir.'parrot.png" alt=""  />
			 <img class="hover_img parrot" src="'.$module_img_dir.'parrot.gif" alt=""  />
		  </div>
		  <h6>Visit us at <a href="https://www.poptin.com/?utm_source=drupal7" target="_blank">poptin.com</a></h6>
	   </div>
	   <div class="modal fade whereis_myid hidediv " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		  <div class="modal-dialog modal-lg" role="document">
			 <div class="modal-content ">
				<h4>Where is my user ID?</h4>
				<div class="myid_list">
				   <div class="row">
					  <div class="col-sm-4">
						 <div class="id_box">
							<div class="id_box_img"><img src="'.$module_img_dir.'where-is-my-id-01.png" alt=""  /></div>
							<p><strong>1.</strong> Go to your dashboard, in the top bar click on "Settings"</p>
						 </div>
						 <!--/id_box-->
					  </div>
					  <!--/col-4-->
					  <div class="col-sm-4">
						 <div class="id_box">
							<div class="id_box_img"><img src="'.$module_img_dir.'where-is-my-id-02.png" alt=""  /></div>
							<p><strong>2.</strong> Click on Profile </p>
						 </div>
						 <!--/id_box-->
					  </div>
					  <!--/col-4-->
					  <div class="col-sm-4">
						 <div class="id_box">
							<div class="id_box_img"><img src="'.$module_img_dir.'where-is-my-id-03.png" alt=""  /></div>
							<p><strong>3.</strong> Copy your user ID </p>
						 </div>
						 <!--/id_box-->
					  </div>
					  <!--/col-4-->
				   </div>
				</div>
				<!--/myid_list-->
				<div class="popup_down">
				   <div class="poptup_button"><a href="javascript:void(0)" class="close_where" data-dismiss="modal" aria-label="Close">Close</a></div>
				</div>
			 </div>
		  </div>
	   </div>
	   <!--/whereis_myid-->
	    <div class="modal fade lookfamiliar hidediv" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
			 <div class="modal-content">
				<img class="poptin-parrot-makingsure-image" src="'.$module_img_dir.'parrot-familiar.png">
				<h4>You look familiar</h4>
				<p>You already have a Poptin account with this email address. </p>
				<div class="popup_down">
				   <div class="poptup_button"><a href="https://app.popt.in/overview" target="_blank">Login</a></div>
				   <a href="javascript:void(0)" class="close_lookfamiliar" data-dismiss="modal" aria-label="Close">I\'ll stay</a>
				</div>
			 </div>
		  </div>
	   </div> 
	   <!--/lookfamiliar-->
	   <div class="modal fade hidediv oopsiewrongemailid" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
			 <div class="modal-content">
				<img class="poptin-parrot-makingsure-image" src="'.$module_img_dir.'parrot-oopsie.png">
				<h4>Oopsie... wrong Email</h4>
				<p>Please enter a valid Email Address.  </p>
				<div class="popup_down">
				   <div class="poptup_button"><a href="javascript:void(0)" class="close_oopsiewrongemailid" data-dismiss="modal" aria-label="Close">Try again</a></div>
				</div>
			 </div>
		  </div>
	   </div>
	   <!--/oopsiewrongid-->
	   <div class="modal fade hidediv oopsiewrongid" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
			 <div class="modal-content">
				<img class="poptin-parrot-makingsure-image" src="'.$module_img_dir.'parrot-oopsie.png">
				<h4>Oopsie... wrong ID</h4>
				<p><a href="javascript:void(0)" class="poptin-lightbox-atag where-is-my-id-inside-lb wheremyid" data-toggle="modal">Where is my user ID?</a></p>
				<div class="popup_down">
				   <div class="poptup_button"><a href="javascript:void(0)"  class="close_oopsiewrongid" data-dismiss="modal" aria-label="Close">Try again</a></div>
				</div>
			 </div>
		  </div>
	   </div>
	   <!--/oopsiewrongid-->
	   <div class="modal fade hidediv " id="deactivate_poptin_popup" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
			 <div class="modal-content">
				<img class="poptin-parrot-makingsure-image" src="'.$module_img_dir.'parrot-making-sure.png">
				<h4>Just making sure</h4>
				<p>Are you sure you want to <br /> remove Poptin? </p>
				<div class="popup_down">
					<span id="deactivate_url" class="hidediv">'.$deactivate_url.'</span>
				   <div class="poptup_button"><a href="javascript:void(0)" class="pplogout1" >Yes</a></div>
				   <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close" class="close_confirm">I\'ll stay</a>
				</div>
			 </div>
		  </div>
	   </div> 	    	 
	   <!--a href="'.$base_url.'/poptinbye/js" class="ppbye ctools-use-modal hidediv">Bye Bye</a>
	   <a href="'.$base_url.'/poptinacctexists/js" class="ppexists ctools-use-modal hidediv">exists</a-->
	   
	   <!--/deactivate_poptin_popup-->
	   <div class="modal hidediv fade byebyeModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
			 <div class="modal-content">
				<img class="poptin-parrot-byebye-image" src="'.$module_img_dir.'parrot-bye-bye.png">
				<h4>Bye Bye</h4>
				<p>Poptin snippet has been <br /> removed. See you around. </p>
				<div class="popup_down">
				   <div class="poptup_button"><a href="javascript:void(0)" class="close_bybye" data-dismiss="modal" aria-label="Close">Close</a></div>
				</div>
			 </div>
		  </div>
	   </div>
	   <!--/byebyeModal-->
	</div>';
	return $str;
}

function is_poptin_user(){
    $result = db_select('poptin', 'p')
            ->fields('p')
            ->execute();
    $num_of_results = $result->rowCount();
	if($num_of_results > 0){
		return true;		
	}else{
		return false;		
	}	
}

function poptin_redirect_link(){
	global $base_url;
	$redirect_link = $base_url;
	$results = db_select('poptin', 'p')
            ->fields('p')
            ->execute();
	foreach($results as $result){
		if(($result->client_id != "") && ($result->token != "")){
			$redirect_link = 'https://app.popt.in/overview';		
		}
	}
	return $redirect_link;
}